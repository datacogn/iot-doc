## 1.获取医疗常识列表

url:/wx/medical/nous

### 请求参数

属性|类型|描述
--|:--|:--
openid|String|唯一id


### 响应参数

属性|类型|描述
--|:--|:--
id|String|id
content|String|医疗常识内容
success|boolean|接口成功或失败，true成功，false失败
msg|String|接口调用返回信息


## 2.添加急救卡

url:/wx/addUserInfo

### 请求参数

属性|类型|描述
--|:--|:--
openid|String|唯一id
userName|String|用户名
birthday|String|出生日期
medicalCond|String|医疗状况
medicalNotes|String|医疗笔记
allergicReaction|String|过敏反应
medicate|String|用药
blood|String|血型
organDonor|int|器官捐赠者，1：是，0否
weight|String|体重
height|String|身高
contact|List|紧急联系人

### 响应参数

属性|类型|描述
--|:--|:--
id|String|id
success|boolean|接口成功或失败，true成功，false失败
msg|String|接口调用返回信息


## 3.获取急救卡列表

ur:/wx/getUserInfo

### 请求参数

属性|类型|描述
--|:--|:--
openid|String|唯一id


### 响应参数



属性|类型|描述
--|:--|:--
success|boolean|接口成功或失败，true成功，false失败
msg|String|接口调用返回信息
urgentCard|List<>|结构同上





## 4.每分钟获取一次用户的心跳频率和呼吸频率统计

ur:/wx/getHeatAndBreath

### 请求参数

属性|类型|描述
--|:--|:--
openid|String|唯一id

### 响应参数

属性|类型|描述
--|:--|:--
success     |boolean|接口成功或失败，true成功，false失败
msg         |String|接口调用返回信息
data        |List|该数据结构，如下面图



属性|类型|描述
--|:--|:--
success     |boolean|接口成功或失败，true成功，false失败
msg         |String|接口调用返回信息
minute      |int|分钟数，从1开始到一天结束
minH        |int|心率最小值
maxH        |int|心率最大值
avgH        |int|心率平均值
heartFlag   |int|心率状态，正常，0：无效，-1:异常
minB        |int|呼吸最小值
maxB        |int|呼吸最大值
avgB        |int|呼吸平均值
breathFlag  |int|呼吸状态 ：正常，0：无效，-1:异常
bedCount    |int|离床次数





## 5.获取离床和睡眠统计，该接口不需要每分钟获取数据

ur:/wx/getBedAndSleep

### 请求参数

属性|类型|描述
--|:--|:--
userId|String|微信登录用户Id

### 响应参数

属性|类型|描述
--|:--|:--
success     |boolean|接口成功或失败，true成功，false失败
msg         |String|接口调用返回信息
sleepQuality    |int|睡眠质量枚举值







