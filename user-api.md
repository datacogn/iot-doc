接口
    1.登录注册接口（上传用户微信code，服务器拿code请求微信服务器获取token等信息，返回用户唯一标识）
    2.获取用户信息接口（返回用户名字、头像、手机号等）
    3.绑定手机号接口
    4.首页医疗检测信息（根据日期，返回某天信息）
    5.医疗常识接口
    6.急救医疗卡信息
    7.添加急救医疗卡信息
    8.商城列表接口
    9.商品详请接口（返回banner轮播图、商品信息、商品详情图）
    10.商品提交订单接口
    11.商品支付接口
    12.商品订单列表接口



## 1.登录注册接口

url:/wx/authUser

### 请求参数

属性|类型|描述
--|:--|:--
wxCode|String|用户微信code


### 响应参数

属性|类型|描述
--|:--|:--
openid|String|用户唯一标识
session_key|String|会话密钥
userName|String|用户名称
success|boolean|接口成功或失败，true成功，false失败
msg|String|接口调用返回信息



## 2.上报微信用户信息(该接口是一个完整的用户信息处理，根据上报数据填入可选的参数即可)

url:/wx/uploadUserInfo

### 请求参数

属性|类型|描述
--|:--|:--
openid|String|用户唯一标识
userName|String|用户名称
tel|String|手机号，绑定手机号接口
sign|String|签名
city|String|城市
address|String|地址
avatar|String|头像

### 响应参数

属性|类型|描述
--|:--|:--
success|boolean|接口成功或失败，true成功，false失败
msg|String|接口调用返回信息




















